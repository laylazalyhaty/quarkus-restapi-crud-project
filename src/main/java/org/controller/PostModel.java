
package org.controller;

import io.smallrye.common.constraint.NotNull;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 *
 * @author asus
 */
public class PostModel {
    @NotNull
    @Schema(required=true,example="Blabla")
    public String title;
    @NotNull
    @Schema(required=true,example="Today going with")
    public String content;
    public String tags;

    public PostModel() {
    }
    
    
}
