package org.controller;

import io.smallrye.common.constraint.NotNull;

import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.DELETE;
import org.domain.Post;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.annotations.jaxrs.PathParam;


@Path("/post")
@Produces(MediaType.APPLICATION_JSON)
public class RestApi {

    @GET
    public Response getList() {
        
        List<Post> post =Post.findAll().list();
        return Response.ok(post).build();
    }
    
    @GET
    @Path("{id}")
    public Response getSingle(@PathParam Long id) {
        
        Post post = Post.findById(id);
        return Response.ok(post).build();
    }
    
    @POST
    @Transactional
    public Response save(@NotNull PostModel model){
        
        Post post= new Post(model.title, model.content, model.tags);
        post.persist();
        
        return Response.ok(post).build();
    }
    
    @PUT
    @Path("{id}")
    @Transactional
    public Response update(@PathParam Long id,@NotNull PostModel model){
        
        Post post= Post.findById(id);
        
        if(post==null){
            throw new WebApplicationException("Post with this Id doesn't exist!",404);
        }
        post.title = model.title;
        post.content = model.content;
        post.tags = model.tags;        
        post.persist();
        
        return Response.ok(post).build();
    }
    
    @DELETE
    @Path("{id}")
    @Transactional
    public Response delete(@PathParam Long id,@NotNull PostModel model){
        
        Post post= Post.findById(id);
        
        if(post==null){
            throw new WebApplicationException("Post with this Id doesn't exist!",404);
        }
        post.delete();
        
        return Response.ok("Delete succesfully!").build();
    }


}


