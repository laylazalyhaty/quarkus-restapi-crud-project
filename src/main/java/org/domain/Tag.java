
package org.domain;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.smallrye.common.constraint.NotNull;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "Tag")
public class Tag extends PanacheEntity{
    @NotNull
    public String label;
    public String posts;

    public Tag() {
    }

    public Tag(String label, String posts) {
        this.label = label;
        this.posts = posts;
    }
    
    
    
}
