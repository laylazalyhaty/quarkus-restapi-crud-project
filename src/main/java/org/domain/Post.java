
package org.domain;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.smallrye.common.constraint.NotNull;
import javax.persistence.Entity;
import javax.persistence.Table;



@Entity
@Table(name = "Post")
public class Post extends PanacheEntity {
    @NotNull
    public String title;
    @NotNull
    public String content;
    public String tags;

    public Post() {
    }
    
    

    public Post(String title, String content, String tags) {
        this.title = title;
        this.content = content;
        this.tags = tags;
    }
    
   
}
